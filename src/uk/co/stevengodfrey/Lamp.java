package uk.co.stevengodfrey;

public class Lamp {
    private String style;
    private boolean battery;
    private int rating;
    private boolean on;

    public Lamp(String style, boolean battery, int rating, boolean on) {
        this.style = style;
        this.battery = battery;
        this.rating = rating;
        this.on = false;
    }

    public void flickSwitch(){
        if (this.on) {
    this.on = false;
    System.out.println("lamp is now off");
}else {
    this.on = true;
    System.out.println("lamp is now on");
}

}

    public String getStyle() {
        return style;
    }

    public boolean isBattery() {
        return battery;
    }

    public int getRating() {
        return rating;
    }

    public boolean isOn() {
        return on;
    }
}
