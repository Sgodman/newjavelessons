package uk.co.stevengodfrey;

public class Main {

    public static void main(String[] args) {
        Wall wall1 = new Wall("west");
        Wall wall2 = new Wall("east");
        Wall wall3 = new Wall("north");
        Wall wall4 = new Wall("south");
        Celing celing = new Celing(200, 234);
        Bed bed = new Bed("table",60,2,1,2);
        Lamp lamp = new Lamp("side", true, 25, false);

        Bedroom bedroom = new Bedroom("Steve's room",wall1, wall2, wall3, wall4, celing, bed, lamp);
        bedroom.getLamp().flickSwitch();
        bedroom.getLamp().flickSwitch();
        bedroom.makeBed();
    }
}
