package uk.co.stevengodfrey;

public class Celing {
    private int height;
    private int colour;

    public Celing(int height, int colour) {
        this.height = height;
        this.colour = colour;
    }

    public int getHeight() {
        return height;
    }

    public int getColour() {
        return colour;
    }
}
